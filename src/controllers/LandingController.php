<?php

use Laconia\Controller;

class LandingController extends Controller
{
    public $pageTitle = null;
    public $message;
    public $user;
    public $isLoggedIn;
    public $csrf;

    public function get()
    {
        $this->isLoggedIn = $this->session->isUserLoggedIn();

        $userId = $this->session->getSessionValue('user_id');
        $this->user = $this->userControl->getUser($userId);
        $this->csrf = $this->session->getSessionValue('csrf');

        $this->view('landing');
    }
}
