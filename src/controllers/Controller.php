<?php

/**
 * Controller Class
 *
 * Connects the database and session models to the front-end views
 */

namespace Laconia;

abstract class Controller
{
    protected $pageTitle;
    protected $message;
    protected $session;
    protected $userControl;
    protected $productCategories;
    protected $products;
    protected $news;

    public function __construct(
        Session $session,
        User $userControl,
        ProductCategories $productCategories,
        Products $products,
        News $news
    )
    {
        $this->session = $session;
        $this->userControl = $userControl;
        $this->productCategories = $productCategories;
        $this->products = $products;
        $this->news = $news;
    }

    public function escape($html)
    {
        return htmlspecialchars($html, ENT_QUOTES | ENT_SUBSTITUTE, 'UTF-8');
    }

    /**
     * Shortcut to retrieve JavaScript file from the /js/ directory.
     * Returns a URL.
     */
    protected function getScript($filename)
    {
        $file = strtolower($filename);

        return PROTOCOL . $_SERVER['HTTP_HOST'] . '/js/' . $file . '.js';
    }

    /**
     * Shortcut to retrieve stylesheet file from the /css/ directory.
     * Returns a URL.
     */
    protected function getStylesheet($filename)
    {
        $file = strtolower($filename);

        return PROTOCOL . $_SERVER['HTTP_HOST'] . '/css/' . $file . '.css';
    }

    /**
     * Shortcut to retrieve image file from the /images/ directory.
     * Returns a URL.
     */
    protected function getImage($filename)
    {
        $file = strtolower($filename);

        return PROTOCOL . $_SERVER['HTTP_HOST'] . '/images/' . $file;
    }

    /**
     * Retrieve a view URL by filename.
     * Requires a file.
     */
    protected function view($view)
    {
        $view = strtolower($view);

        require_once $_SERVER['DOCUMENT_ROOT'] . '/../src/views/' . $view . '.view.php';
    }

    /**
     * Check if the current page is the Index.
     * Returns a Boolean.
     */
    protected function isIndex()
    {
        $redirect = ltrim($_SERVER['REDIRECT_URL'], '/');

        return $redirect === '';
    }

    /**
     * Check if the current page is specified page.
     * Returns a Boolean.
     */
    protected function isPage($view)
    {
        $view = strtolower($view);

        $redirect = ltrim($_SERVER['REDIRECT_URL'], '/');

        return $redirect === $view;
    }

    /**
     * Redirects to the specified page.
     */
    protected function redirect($view)
    {
        $view = strtolower($view);

        header('Location: /' . $view);
        exit;
    }

    /**
     * Securely hash a password.
     * Returns hashed password.
     */
    protected function encryptPassword($password)
    {
        $passwordHash = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));

        return $passwordHash;
    }

    /**
     * Vertify a submitted password against existing password.
     * Return a Boolean.
     */
    protected function verifyPassword($submittedPassword, $password)
    {
        $validPassword = password_verify($submittedPassword, $password);

        return $validPassword;
    }

    /**
     * Check if a username is in the list of disallowed usernames.
     * Return a Boolean.
     */
    protected function isApprovedUsername($username)
    {
        $approved = in_array($username, DISALLOWED_USERNAMES) ? false : true;

        return $approved;
    }

    /**
     * Check if username is empty, and make sure it only contains
     * alphanumeric characters, numbers, dashes, and underscores.
     * Return an error or null.
     */
    protected function validateUsername($username)
    {
        if (!empty($username)) {
            if (strlen($username) < '3') {
                $this->errors[] = USERNAME_TOO_SHORT;
            }
            if (strlen($username) > '50') {
                $this->errors[] = USERNAME_TOO_LONG;
            }
            // Match a-z, A-Z, 1-9, -, _.
            if (!preg_match("/^[a-zA-Z\d-_]+$/i", $username)) {
                $this->errors[] = USERNAME_CONTAINS_DISALLOWED;
            }
        } else {
            $this->errors[] = USERNAME_MISSING;
        }
    }

    /**
     * Check if password is empty, and make sure it conforms
     * to password security standards.
     * Return an error or null.
     */
    protected function validatePassword($password)
    {
        if (!empty($password)) {
            if (strlen($password) < '8') {
                $this->errors[] = PASSWORD_TOO_SHORT;
            }
            if (!preg_match("#[0-9]+#", $password)) {
                $this->errors[] = PASSWORD_NEEDS_NUMBER;
            }
            if (!preg_match("#[A-Z]+#", $password)) {
                $this->errors[] = PASSWORD_NEEDS_UPPERCASE;
            }
            if (!preg_match("#[a-z]+#", $password)) {
                $this->errors[] = PASSWORD_NEEDS_LOWERCASE;
            }
        } else {
            $this->errors[] = PASSWORD_MISSING;
        }
    }

    /**
     * Check if email is empty, and test it against PHP built in
     * email validation.
     * Return an error or null.
     */
    protected function validateEmail($email)
    {
        if (!empty($email)) {
            // Remove all illegal characters from email
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);

            // Validate e-mail
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $this->errors[] = EMAIL_NOT_VALID;
            }
        } else {
            $this->errors[] = EMAIL_MISSING;
        }
    }

    /**
     * Get list of errors from validation.
     * Return a string.
     */
    protected function getErrors($errors)
    {
        foreach ($errors as $error) {
            $this->errorList .= $error . "\n";
        }
        return $this->errorList;
    }

    protected function formatPrice($price)
    {
        return number_format($price, 0, ',', '.') . '<sup>đ</sup>';
    }

    protected function uploadFile($file, $index = null)
    {
        $file_name = isset($index) ? $file['name'][(int)$index] : $file['name'];
        $tmp_name = isset($index) ? $file['tmp_name'][(int)$index] : $file['tmp_name'];
        $file_size = isset($index) ? $file['size'][(int)$index] : $file['size'];

        $folder = date('Y/m/');
        $target_dir = DIR . "/uploads/" . $folder;

        $new_file_name = pathinfo($file_name, PATHINFO_FILENAME) . '_' . time() . '.' . pathinfo($file_name, PATHINFO_EXTENSION);

        $new_target_file = $target_dir . basename($new_file_name);
        $imageFileType = strtolower(pathinfo($new_target_file, PATHINFO_EXTENSION));

        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0777, true);
        }

        $check = getimagesize($tmp_name);
        if ($check !== false) {
            //echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            //echo "File is not an image.";
            $uploadOk = 0;
        }

        // Check file size
        if ($file_size > 50000000) {
            //echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }

        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif") {
            //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            //echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($tmp_name, $new_target_file)) {
                return $folder . basename($new_file_name);
            }
        }
        return false;
    }

    protected function truncate($string, $length = 80)
    {
        $strlen = strlen($this->remove_unicode($string));
        if ($strlen <= $length) {
            return $string;
        } else {
            mb_internal_encoding("UTF-8");
            return mb_substr($string, 0, $length) . "...";
        }
    }

    protected function remove_unicode($_text)
    {
        $text = htmlspecialchars(trim(strip_tags($_text)));
        $chars = array("a", "A", "e", "E", "o", "O", "u", "U", "i", "I", "d", "D", "y", "Y");
        $uni [0] = array("á", "à", "ạ", "ả", "ã", "â", "ấ", "ầ", "ậ", "ẩ", "ẫ", "ă", "ắ", "ằ", "ặ", "ẳ", "� �", "ả", "á", "ặ");
        $uni [1] = array("Á", "À", "Ạ", "Ả", "Ã", "Â", "Ấ", "Ầ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ắ", "Ằ", "Ặ", "Ẳ", "� �", "Ạ", "Á", "À", "Ã", "Ả");
        $uni [2] = array("é", "è", "ẹ", "ẻ", "ẽ", "ê", "ế", "ề", "ệ", "ể", "ễ", "ệ");
        $uni [3] = array("É", "È", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ế", "Ề", "Ệ", "Ể", "Ễ", "É", "Ẽ");
        $uni [4] = array("ó", "ò", "ọ", "ỏ", "õ", "ô", "ố", "ồ", "ộ", "ổ", "ỗ", "ơ", "ớ", "ờ", "ợ", "ở", "� �");
        $uni [5] = array("Ó", "Ò", "Ọ", "Ỏ", "Õ", "Ô", "Ố", "Ồ", "Ộ", "Ổ", "Ỗ", "Ơ", "Ớ", "Ờ", "Ợ", "Ở", "� �", "Ọ", "Õ");
        $uni [6] = array("ú", "ù", "ụ", "ủ", "ũ", "ư", "ứ", "ừ", "ự", "ử", "ữ", "ù");
        $uni [7] = array("Ú", "Ù", "Ụ", "Ủ", "Ũ", "Ư", "Ứ", "Ừ", "Ự", "Ử", "Ữ", "Ú", "Ũ");
        $uni [8] = array("í", "ì", "ị", "ỉ", "ĩ");
        $uni [9] = array("Í", "Ì", "Ị", "Ỉ", "Ĩ", "Ỉ", "Ì", "Ĩ", "Í", "Ị");
        $uni [10] = array("đ");
        $uni [11] = array("Đ");
        $uni [12] = array("ý", "ỳ", "ỵ", "ỷ", "ỹ");
        $uni [13] = array("Ý", "Ỳ", "Ỵ", "Ỷ", "Ỹ");
        for ($i = 0; $i <= 13; $i++) {
            $text = str_replace($uni[$i], $chars[$i], $text);
        }
        return $text;
    }
}
