<?php

use Laconia\Controller;

class AjaxController extends Controller
{
    public $message;
    public $csrf;
    public $cat;
    public $products_in_cat;
    public $search_results;

    public function getProductsByCat()
    {
        $get = filter_get();
        $this->cat = $this->productCategories->get((int)$get['id']);
        $this->products_in_cat = $this->products->getByCategoriesRelation((int)$get['id'], 4);
        $this->view('ajax/product_by_cat');
    }

    public function search()
    {
        $get = filter_get();

        $catIds = $this->products->getCatIdsRelation((int)$get['cat'], false);

        $orderBy = "ORDER BY id DESC";
        $sortBy = isset($get['sortby']) ? explode(":", $get['sortby']) : [];
        if (!empty($sortBy)) $orderBy = "ORDER BY {$sortBy[0]} {$sortBy[1]}";


        $q = htmlspecialchars_decode($get['q']);
        $q = explode('AND', $q);
        $newArr = [];

        if (!empty($q)) {
            $name = '';
            foreach ($q as $item) {
                $item = str_replace("(", "", trim($item));
                $item = str_replace(")", "", trim($item));

                if (strpos($item, ":") !== false) {
                    $exp = explode(":", $item);
                    $name = $exp[0];
                    $item = $exp[1];
                }

                $item = str_replace("OR", "OR " . $name, $item);

                array_push($newArr, $name . ' ' . trim($item));
            }
        }

        $where = "category_id IN (" . implode(',', $catIds) . ")";
        $newArr = implode(' AND ', $newArr);
        if (trim($newArr) !== '') {
            $where .= " AND " . $newArr;
        }

        $query = "SELECT * FROM products WHERE {$where} AND status = 1 {$orderBy}";
        $this->search_results = $this->products->search($query);

        $this->view('ajax/search');

    }
}
