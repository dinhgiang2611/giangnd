<?php

use Laconia\Controller;

class HomeController extends Controller
{
    public $pageTitle = null;
    public $message;
    public $user;
    public $isLoggedIn;
    public $csrf;
    public $promotes;
    public $firstCategory;
    public $getCatsWithLimit;
    public $products_dh;
    public $products_mg;
    public $h_news;

    public function get()
    {
        $this->isLoggedIn = $this->session->isUserLoggedIn();
        $userId = $this->session->getSessionValue('user_id');
        $this->user = $this->userControl->getUser($userId);
        $this->csrf = $this->session->getSessionValue('csrf');

        $this->pageTitle = 'Hàng Nhật Bãi điều hòa, tủ lạnh các loại';
        $this->firstCategory = $this->productCategories->getFirstCategory();
        $this->getCatsWithLimit = $this->productCategories->getWithLimit(4);
        $this->promotes = $this->products->getPromotes();
        $this->products_dh = $this->products->getByCategoriesRelation(1, 4);
        $this->products_mg = $this->products->getByCategoriesRelation(10, 4);
        $this->h_news = $this->news->getWithLimit(5);

        $this->view('home');
    }
}
