<?php

use Laconia\Controller;

class ProductController extends Controller
{
    public $pageTitle = '';
    public $message;
    public $csrf;
    public $product;
    public $best_sellers;
    public $related;

    public function get()
    {
        $get = filter_get();
        $this->product = $this->products->get($get['id']);
        $this->pageTitle = $this->product['name'];
        $this->news = $this->news->getWithLimit(3);
        $this->best_sellers = $this->products->getBestSellers(3);
        $this->related = $this->products->getRelatedProducts((int)$this->product['id'], (int)$this->product['category_id'], 6);

        $this->view('product');
    }
}
