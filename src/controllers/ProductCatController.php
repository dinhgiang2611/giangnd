<?php

use Laconia\Controller;

class ProductCatController extends Controller
{
    public $pageTitle = '';
    public $message;
    public $csrf;
    public $id;
    public $product_cat;
    public $product_list;

    public function getProducts()
    {
        $get = filter_get();
        $this->id = (int)$get['id'];
        $this->product_cat = $this->productCategories->get($this->id);
        $this->pageTitle = $this->product_cat['name'];
        $this->product_list = $this->products->getByCategory($this->id);

        $this->view('product-cat');
    }
}
