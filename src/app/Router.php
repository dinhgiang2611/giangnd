<?php

/**
 * Router Class
 * @link https://github.com/laracasts/The-PHP-Practitioner-Full-Source-Code/blob/master/core/Router.php
 */

use Laconia\Session;
use Laconia\User;
use Laconia\ProductCategories;
use Laconia\Products;
use Laconia\News;

class Router
{
    /**
     * All registered routes.
     */
    public $routes = [
        'GET' => [],
        'POST' => []
    ];

    /**
     * Load a user's routes file.
     */
    public static function load($file)
    {
        $router = new static;

        require $file;
        return $router;
    }

    /**
     * Register a GET route.
     */
    public function get($uri, $controller)
    {
        $this->routes['GET'][$uri] = $controller;
    }

    /**
     * Register a POST route.
     */
    public function post($uri, $controller)
    {
        $this->routes['POST'][$uri] = $controller;
    }

    /**
     * Load the requested URI's associated controller method.
     */
    public function direct($uri, $requestType)
    {
        $userControl = new User;
        $username = $userControl->getUserByUsername($uri);

        // If uri contains edit, go to edit controller
        if (($pos = strpos($uri, '/')) !== false) {
            if (strpos($uri, 'edit') !== false) {
                $uri = 'edit';
            }
        } // Gather all users from the database and compare against uri
        elseif ($username) {
            $uri = 'user-profile';
        }

        if (array_key_exists($uri, $this->routes[$requestType])) {
            return $this->callAction(
                ...explode('@', $this->routes[$requestType][$uri])
            );
        } else {
            return $this->callAction(
                ...explode('@', $this->routes[$requestType]['404'])
            );
        }

        throw new Exception('No route defined for this URI.');
    }

    /**
     * Load and call the relevant controller action.
     */
    protected function callAction($controller, $action)
    {
        $session = new Session;
        $userControl = new User;
        $productCategories = new ProductCategories;
        $products = new Products;
        $news = new News;

        $controller = new $controller(
            $session,
            $userControl,
            $productCategories,
            $products,
            $news
        );

        if (!method_exists($controller, $action)) {
            throw new Exception(
                "{$controller} does not respond to the {$action} action."
            );
        }
        return $controller->$action();
    }
}
