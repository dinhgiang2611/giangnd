<?php
/**
 * @var Router $router
 */

$router->get('', 'HomeController@get');

$router->get('login', 'LoginController@get');
$router->post('login', 'LoginController@post');

$router->get('register', 'RegisterController@get');
$router->post('register', 'RegisterController@post');

$router->get('logout', 'LogoutController@get');

$router->get('forgot-password', 'ForgotPasswordController@get');
$router->post('forgot-password', 'ForgotPasswordController@post');

$router->get('reset-password', 'ResetPasswordController@get');
$router->post('reset-password', 'ResetPasswordController@post');

$router->get('create-password', 'CreatePasswordController@get');
$router->post('create-password', 'CreatePasswordController@post');

$router->get('users', 'UsersController@get');
$router->get('user-profile', 'UserProfileController@get');

$router->get('404', 'ExceptionNotFoundController@get');

$router->get('ajax/load-product-by-cat', 'AjaxController@getProductsByCat');

$router->get('about', 'PageController@about');
$router->get('contact', 'PageController@contact');
$router->get('news', 'NewsController@get');

$router->get('product-cat', 'ProductCatController@getProducts');
$router->get('product', 'ProductController@get');

$router->get('search', 'AjaxController@search');