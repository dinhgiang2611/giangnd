<?php

namespace Laconia;

class Products extends Model
{
    public function get($id)
    {
        $query = "SELECT * FROM products WHERE id = :id";

        $this->db->query($query);
        $this->db->bind(':id', $id);

        $results = $this->db->result();
        return $results;
    }

    public function getAll()
    {
        $query = "SELECT * FROM products";

        $this->db->query($query);

        $results = $this->db->resultset();
        return $results;
    }

    public function search($query)
    {
        $this->db->query($query);
        $results = $this->db->resultset();
        return $results;
    }

    public function sortBy()
    {
        $orderBy = "ORDER BY id DESC";
        if (isset($_GET['sortby']) && !empty($_GET['sortby'])) {
            $sortBy = explode(":", $_GET['sortby']);
            $orderBy = "ORDER BY {$sortBy[0]} {$sortBy[1]}";
        }
        return $orderBy;
    }

    public function getByCategory($category_id, $limit = 10)
    {
        $orderBy = $this->sortBy();
        $catRelationIds = $this->getCatIdsRelation($category_id, false);
        $catRelationIds = implode(",", $catRelationIds);

        $query = "SELECT * FROM products WHERE category_id IN ({$catRelationIds}) AND status = :status {$orderBy} LIMIT {$limit}";

        $this->db->query($query);
        $this->db->bind(':status', 1);

        $results = $this->db->resultset();
        return $results;
    }

    public function getPromotes($limit = 10)
    {
        $query = "SELECT * FROM products WHERE sale_price != :sale_price AND status = :status LIMIT {$limit}";

        $this->db->query($query);
        $this->db->bind(':sale_price', 0);
        $this->db->bind(':status', 1);

        $results = $this->db->resultset();
        return $results;
    }

    public function getByCategoriesRelation($category_id, $limit = 10)
    {
        $catRelationIds = $this->getCatIdsRelation($category_id);
        $catRelationIds = implode(",", $catRelationIds);
        $query = "SELECT * FROM products WHERE category_id IN ({$catRelationIds}) AND status = :status LIMIT {$limit}";

        $this->db->query($query);
        $this->db->bind(':status', 1);

        $results = $this->db->resultset();
        return $results;
    }

    public function getCatIdsRelation($category_id, $get_parent = true)
    {
        $catIds = array($category_id);

        //find children
        $query = "SELECT * FROM product_categories WHERE parent_id = :parent_id";
        $this->db->query($query);
        $this->db->bind(':parent_id', $category_id);
        $children = $this->db->resultset();

        if (!empty($children)) {
            foreach ($children as $child) {
                array_push($catIds, $child['id']);
            }
        }

        if ($get_parent) {
            //find parents
            $query = "SELECT * FROM product_categories WHERE id = :id AND parent_id != 0";
            $this->db->query($query);
            $this->db->bind(':id', $category_id);
            $parent = $this->db->result();

            if (!empty($parent)) {
                array_push($catIds, $parent['parent_id']);
            }
        }

        return $catIds;
    }

    public function getProductImages($id)
    {
        $query = "SELECT * FROM product_images WHERE product_id = :product_id";

        $this->db->query($query);
        $this->db->bind(':product_id', $id);

        $results = $this->db->resultset();
        return $results;
    }

    public function getBestSellers($limit = 10)
    {
        $query = "SELECT * FROM products ORDER BY purchases DESC LIMIT {$limit}";

        $this->db->query($query);

        $results = $this->db->resultset();
        return $results;
    }

    public function getRelatedProducts($product_id, $category_id, $limit = 10)
    {
        $query = "SELECT * FROM products WHERE id != :id AND category_id = :category_id LIMIT {$limit}";

        $this->db->query($query);
        $this->db->bind(':id', $product_id);
        $this->db->bind(':category_id', $category_id);

        $results = $this->db->resultset();
        return $results;
    }

}