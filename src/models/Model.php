<?php

/**
 * Model Class
 * 
 * Create a new instance of the Database class.
 */
namespace Laconia;

abstract class Model
{
	protected $db;

	public function __construct()
	{
		$this->db = new Database();
	}
}
