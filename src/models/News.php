<?php

namespace Laconia;

class News extends Model
{
    public function getAll()
    {
        $query = "SELECT * FROM news";

        $this->db->query($query);

        $results = $this->db->resultset();
        return $results;
    }

    public function getWithLimit($limit = 10)
    {
        $query = "SELECT * FROM news LIMIT {$limit}";

        $this->db->query($query);

        $results = $this->db->resultset();
        return $results;
    }

}