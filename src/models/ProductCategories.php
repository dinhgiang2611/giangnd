<?php

namespace Laconia;

class ProductCategories extends Model
{
    public function get($id)
    {
        $query = "SELECT * FROM product_categories WHERE id = :id";

        $this->db->query($query);
        $this->db->bind(':id', $id);

        $results = $this->db->result();
        return $results;
    }

    public function getParents()
    {
        $query = "SELECT * FROM product_categories WHERE parent_id = :parent_id AND status = :status";

        $this->db->query($query);
        $this->db->bind(':parent_id', 0);
        $this->db->bind(':status', 1);

        $results = $this->db->resultset();
        return $results;
    }

    public function getChildren($id)
    {
        $query = "SELECT * FROM product_categories WHERE parent_id = :parent_id AND status = :status";

        $this->db->query($query);
        $this->db->bind(':parent_id', $id);
        $this->db->bind(':status', 1);

        $results = $this->db->resultset();
        return $results;
    }

    public function getFirstCategory()
    {
        $query = "SELECT * FROM product_categories WHERE parent_id = :parent_id AND status = :status LIMIT 1";

        $this->db->query($query);
        $this->db->bind(':parent_id', 0);
        $this->db->bind(':status', 1);

        $results = $this->db->result();
        return $results;
    }

    public function getWithLimit($limit = 10)
    {
        $query = "SELECT * FROM product_categories WHERE status = :status LIMIT {$limit}";

        $this->db->query($query);
        $this->db->bind(':status', 1);

        $results = $this->db->resultset();
        return $results;
    }

}