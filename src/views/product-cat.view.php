<?php include __DIR__ . '/partials/header.php'; ?>

    <div class="page-collection">
        <section class="bread-crumb">
            <span class="crumb-border"></span>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 a-left">
                        <ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                            <li class="home">
                                <a itemprop="url" href="/"><span itemprop="title">Trang chủ</span></a>
                                <span class="mr_lr"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                            </li>
                            <li><strong><span itemprop="title"> <?= $this->product_cat['name']; ?></span></strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="container">
        <div class="row">
            <section class="main_container collection col-lg-9 col-md-9 col-sm-12 col-lg-push-3 col-md-push-3">
                <?php include __DIR__ . '/partials/product-cat/title.php'; ?>
                <div class="category-products products">
                    <?php include __DIR__ . '/partials/product-cat/sort.php'; ?>
                    <?php include __DIR__ . '/partials/product-cat/grid.php'; ?>
                </div>
            </section>

            <?php include __DIR__ . '/partials/left-product-cat.php'; ?>

            <div id="open-filters" class="open-filters hidden-lg hidden-md">
			<span class="fter">
				<i class="fa fa-filter"></i>
			</span>
            </div>
        </div>
    </div>


    <script src="<?php echo $this->getScript('search_filter'); ?>?<?= time(); ?>"></script>
    <script>
        var filter = new Laconia.SearchFilter();

        var selectedSortby;
        var selectedViewData = "data";
        var currentUrl = window.location.href;
        var catId = '<?=$this->product_cat["id"];?>';

        function selectFilterByCurrentQuery() {
            var isFilter = false;
            var url = window.location.href;
            var queryString = decodeURI(window.location.search);
            var filters = queryString.match(/\(.*?\)/g);
            var page = 0;
            if (filters && filters.length > 0) {
                filters.forEach(function (item) {
                    item = item.replace(/\(\(/g, "(");
                    var element = $(".aside-content input[value='" + item + "']");
                    element.attr("checked", "checked");
                    _toggleFilter(element);
                });

                isFilter = true;
            }
            var sortOrder = getParameter(url, "sortby");
            if (sortOrder) {
                _selectSortby(sortOrder);
            }
            if (isFilter) {
                doSearch(page);
                renderFilterdItems();
            }
        }

        function getParameter(url, name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(url);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        function replaceUrlParam(url, paramName, paramValue) {
            if (paramValue == null) {
                paramValue = '';
            }
            var pattern = new RegExp('\\b(' + paramName + '=).*?(&|#|$)');
            if (url.search(pattern) >= 0) {
                return url.replace(pattern, '$1' + paramValue + '$2');
            }
            url = url.replace(/[?#]$/, '');
            return url + (url.indexOf('?') > 0 ? '&' : '?') + paramName + '=' + paramValue;
        }

        function sortby(sort) {
            switch (sort) {
                case "price-asc":
                    selectedSortby = "sale_price:asc";
                    break;
                case "price-desc":
                    selectedSortby = "sale_price:desc";
                    break;
                case "alpha-asc":
                    selectedSortby = "name:asc";
                    break;
                case "alpha-desc":
                    selectedSortby = "name:desc";
                    break;
                case "created-desc":
                    selectedSortby = "date:desc";
                    break;
                case "created-asc":
                    selectedSortby = "date:asc";
                    break;
                default:
                    selectedSortby = "";
                    break;
            }

            doSearch(1);
        }

        function resortby(sort) {
            switch (sort) {
                case "sale_price:asc":
                    tt = "Giá tăng dần";
                    break;
                case "sale_price:desc":
                    tt = "Giá giảm dần";
                    break;
                case "name:asc":
                    tt = "A → Z";
                    break;
                case "name:desc":
                    tt = "Z → A";
                    break;
                case "date:desc":
                    tt = "Hàng mới nhất";
                    break;
                case "date:asc":
                    tt = "Hàng cũ nhất";
                    break;
                default:
                    tt = "Sắp xếp";
                    break;
            }
            $('#sort-by > ul > li > span').html(tt);
        }

        function _selectSortby(sort) {
            resortby(sort);
            switch (sort) {
                case "price-asc":
                    selectedSortby = "sale_price:asc";
                    break;
                case "price-desc":
                    selectedSortby = "sale_price:desc";
                    break;
                case "alpha-asc":
                    selectedSortby = "name:asc";
                    break;
                case "alpha-desc":
                    selectedSortby = "name:desc";
                    break;
                case "created-desc":
                    selectedSortby = "date:desc";
                    break;
                case "created-asc":
                    selectedSortby = "date:asc";
                    break;
                default:
                    selectedSortby = sort;
                    break;
            }
        }

        function clearAllFiltered() {
            filter = new Laconia.SearchFilter();
            $(".filter-container__selected-filter-list ul").html("");
            $(".filter-container input[type=checkbox]").attr('checked', false);
            $(".filter-container__selected-filter").hide();
            doSearch(1);
        }

        function toggleFilter(e) {
            _toggleFilter(e);
            renderFilterdItems();
            doSearch(1);
        }

        function doSearch(page, options) {
            if (!options) options = {};
            $('.aside.aside-mini-products-list.filter').removeClass('active');
            awe_showPopup('.loading');
            filter.search({
                view: selectedViewData,
                page: page,
                sortby: selectedSortby,
                cat: catId,
                success: function (html) {
                    var $html = $(html);
                    var $categoryProducts = $($html[0]);
                    $(".products-view").html($categoryProducts.html());
                    pushCurrentFilterState({sortby: selectedSortby, page: page});
                    awe_hidePopup('.loading');
                    awe_lazyloadImage();

                    $('.add_to_cart').click(function (e) {
                        e.preventDefault();
                        var $this = $(this);
                        var form = $this.parents('form');
                        $.ajax({
                            type: 'POST',
                            url: '/cart/add.js',
                            async: false,
                            data: form.serialize(),
                            dataType: 'json',
                            error: addToCartFail,
                            beforeSend: function () {
                                if (window.theme_load == "icon") {
                                    awe_showLoading('.btn-addToCart');
                                } else {
                                    awe_showPopup('.loading');
                                }
                            },
                            success: addToCartSuccess,
                            cache: false
                        });
                    });
                    $('html, body').animate({
                        scrollTop: $('.category-products').offset().top
                    }, 0);
                    resortby(selectedSortby);
                    if (window.BPR !== undefined) {
                        return window.BPR.initDomEls(), window.BPR.loadBadges();
                    }
                }
            });
        }

        function pushCurrentFilterState(options) {
            if (!options) options = {};
            var url = filter.buildSearchUrl(options);
            var queryString = url.slice(url.indexOf('?'));
            if (currentUrl.indexOf('?')) {
                queryString = queryString.replace("?", "&");
            }
            if (selectedViewData == 'data_list') {
                queryString = queryString + '&view=list';
            } else {
                queryString = queryString + '&view=grid';
            }
            pushState(queryString);
        }

        function pushState(url) {
            if (currentUrl.indexOf("&q=") !== -1) {
                currentUrl = currentUrl.substring(0, currentUrl.indexOf("&q="));
            }
            window.history.pushState({
                turbolinks: true,
                url: url
            }, null, currentUrl + url)
        }

        function _toggleFilter(e) {
            var $element = $(e);
            var group = $element.attr("data-group");
            var field = $element.attr("data-field");
            var text = $element.attr("data-text");
            var value = $element.attr("value");
            var operator = $element.attr("data-operator");
            var filterItemId = $element.attr("id");

            if (!$element.is(':checked')) {
                filter.deleteValue(group, field, value, operator);
            } else {
                filter.addValue(group, field, value, operator);
            }

            $(".catalog_filters li[data-handle='" + filterItemId + "']").toggleClass("active");
        }

        function renderFilterdItems() {
            var $container = $(".filter-container__selected-filter-list ul");
            $container.html("");

            $(".filter-container input[type=checkbox]").each(function (index) {
                if ($(this).is(':checked')) {
                    var id = $(this).attr("id");
                    var name = $(this).closest("label").text();
                    addFilteredItem(name, id);
                }
            });

            if ($(".aside-content input[type=checkbox]:checked").length > 0)
                $(".filter-container__selected-filter").show();
            else
                $(".filter-container__selected-filter").hide();
        }

        function addFilteredItem(name, id) {
            var filteredItemTemplate = "<li class='filter-container__selected-filter-item' for='{3}'><a href='javascript:void(0)' onclick=\"{0}\"><i class='fa fa-close'></i> {1}</a></li>";
            filteredItemTemplate = filteredItemTemplate.replace("{0}", "removeFilteredItem('" + id + "')");
            filteredItemTemplate = filteredItemTemplate.replace("{1}", name);
            filteredItemTemplate = filteredItemTemplate.replace("{3}", id);
            var $container = $(".filter-container__selected-filter-list ul");
            $container.append(filteredItemTemplate);
        }

        function removeFilteredItem(id) {
            $(".filter-container #" + id).trigger("click");
        }

        $(document).ready(function () {
            $(window).on('popstate', function () {
                location.reload(true);
            });
            selectFilterByCurrentQuery();
            $('.filter-group .filter-group-title').click(function (e) {
                $(this).parent().toggleClass('active');
            });

            $('.filter-mobile').click(function (e) {
                $('.aside.aside-mini-products-list.filter').toggleClass('active');
            });

            $('#show-admin-bar').click(function (e) {
                $('.aside.aside-mini-products-list.filter').toggleClass('active');
            });

            $('.filter-container__selected-filter-header-title').click(function (e) {
                $('.aside.aside-mini-products-list.filter').toggleClass('active');
            });
        });
    </script>

<?php include __DIR__ . '/partials/footer.php'; ?>