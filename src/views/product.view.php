<?php include __DIR__ . '/partials/header.php'; ?>

<section class="bread-crumb">
    <span class="crumb-border"></span>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 a-left">
                <ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <li class="home">
                        <a itemprop="url" href="/"><span itemprop="title">Trang chủ</span></a>
                        <span class="mr_lr"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                    </li>

                    <li>
                        <a itemprop="url" href="/products"><span itemprop="title">Sản phẩm</span></a>
                        <span class="mr_lr"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                    </li>

                    <li>
                        <strong><span itemprop="title"><?= $this->product['name']; ?></span></strong>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="product page-product" itemscope="" itemtype="https://schema.org/Product">
    <div class="container">
        <div class="row">
            <div class="details-product">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="rows row-width">
                        <form enctype="multipart/form-data" id="add-to-cart-form" action="/cart/add" method="post"
                              class="row form-width form-inline">

                            <?php include __DIR__ . '/partials/product/images.php'; ?>
                            <?php include __DIR__ . '/partials/product/details.php'; ?>
                            <?php include __DIR__ . '/partials/product/right-product.php'; ?>

                        </form>

                        <?php include __DIR__ . '/partials/product/tabs.php'; ?>

                    </div>
                </div>
            </div>

            <?php include __DIR__ . '/partials/product/related.php'; ?>

        </div>
    </div>
</section>

<link href="<?= $this->getStylesheet('lightbox'); ?>" rel="stylesheet">
<script src="<?php echo $this->getScript('jquery.elevatezoom308.min'); ?>"></script>
<script src="<?php echo $this->getScript('jquery.prettyphoto.min005e'); ?>"></script>
<script src="<?php echo $this->getScript('jquery.prettyphoto.init.min367a'); ?>"></script>

<script>
    var variantsize = true;
    var productOptionsSize = 1;
    var optionsFirst = 'Màu sắc';
    var cdefault = 0;

    $(document).ready(function () {
        /*** xử lý active thumb -- ko variant ***/
        var thumbLargeimg = $('.details-product .large-image a img').attr('src');
        var url = [];

        $('#gallery_01 .item').each(function () {
            var srcImg = '';
            $(this).find('a img').each(function () {
                var current = $(this);
                if (current.children().size() > 0) {
                    return true;
                }
                srcImg += $(this).attr('src');
            });
            url.push(srcImg);
            var srcimage = $(this).find('a').attr('data-image');
            if (srcimage == thumbLargeimg) {
                $(this).find('a').addClass('active');
            } else {
                $(this).find('a').removeClass('active');
            }
        });
        if ($(window).width() < 1200) {
            $('#gallery_01 .item a').click(function () {
                $('#gallery_01 .item a').not(this).removeClass('active');
                $(this).addClass('active')
            });
        }

        $('.input_number_product button').click(function () {
            var qty = $(".input_number_product input").val();
            $('.buy-now').attr('data-qty', qty)
        })

        $('.input_number_product input').on('change paste keyup', function () {
            var qty = $(this).val();
            $('.buy-now').attr('data-qty', qty)
        })

        $('.details-pro .buy-now').click(function () {
            var id = $(this).attr('data-id');
            var qty = $(this).attr('data-qty');
            var checkout = '/cart/' + id + ':' + qty;
            window.location.href = checkout;
        })

        var checknop = $('a.large_image_url.checkurl').attr('href');
        $('.offset_nop a.check_data_img').each(function () {
            var checknop2 = $(this).attr('href');
            var chtml = $(this).contents();
            if (checknop == checknop2) {
                $(this).removeAttr('data-rel');
            }
        });
    });

    var ww = $(window).width();


    var selectCallback = function (variant, selector) {
        if (variant) {
            var form = jQuery('#' + selector.domIdPrefix).closest('form');

            for (var i = 0, length = variant.options.length; i < length; i++) {

                var radioButton = form.find('.swatch[data-option-index="' + i + '"] :radio[value="' + variant.options[i] + '"]');
                if (radioButton.size()) {
                    radioButton.get(0).checked = true;
                }
            }
        }

        /*begin variant image*/
        if (variant && variant.image) {
            var originalImage = jQuery(".large-image img");
            var newImage = variant.image;
            var element = originalImage[0];
            Bizweb.Image.switchImage(newImage, element, function (newImageSizedSrc, newImage, element) {

                jQuery(element).parents('a').attr('href', newImageSizedSrc);

                jQuery(element).attr('src', newImageSizedSrc);

                if (ww >= 1200) {
                    $("#zoom_01").data('zoom-image', newImageSizedSrc).elevateZoom({
                        responsive: true,
                        gallery: 'gallery_01',
                        cursor: 'pointer',
                        galleryActiveClass: "active"
                    });
                }

                !function (a) {
                    a(function () {
                        a("a.zoom").prettyPhoto({
                            hook: "data-rel",
                            social_tools: !1,
                            theme: "pp_woocommerce",
                            horizontal_padding: 20,
                            opacity: .8,
                            deeplinking: !1
                        }), a("a[data-rel^='prettyPhoto']").prettyPhoto({
                            hook: "data-rel",
                            social_tools: !1,
                            theme: "pp_woocommerce",
                            horizontal_padding: 20,
                            opacity: .8,
                            deeplinking: !1
                        })
                    })
                }(jQuery);
            });


            $('.large-image .checkurl').attr('href', $(this).attr('src'));


            if ($(window).width() > 1200) {
                setTimeout(function () {
                    $('.zoomContainer').remove();
                    $('#zoom_01').elevateZoom({
                        gallery: 'gallery_01',
                        zoomWindowOffetx: 20,
                        easing: true,
                        scrollZoom: true,
                        zoomWindowWidth: 400,
                        zoomWindowHeight: 400,
                        cursor: 'pointer',
                        galleryActiveClass: 'active'
                    });
                }, 300);
            }

        }

        /*end of variant image*/
    };


    $('#gallery_01 img').click(function (e) {
        e.preventDefault();
        $('.large-image img').attr('src', $(this).parent().attr('data-zoom-image'));

        $('.large-image .checkurl').attr('href', $(this).parent().attr('data-image'));

        !function (a) {
            a(function () {
                a("a.zoom").prettyPhoto({
                    hook: "data-rel",
                    social_tools: !1,
                    theme: "pp_woocommerce",
                    horizontal_padding: 20,
                    opacity: .8,
                    deeplinking: !1
                }), a("a[data-rel^='prettyPhoto']").prettyPhoto({
                    hook: "data-rel",
                    social_tools: !1,
                    theme: "pp_woocommerce",
                    horizontal_padding: 20,
                    opacity: .8,
                    deeplinking: !1
                })
            })
        }(jQuery);
    });

    if ($(window).width() > 1200) {
        setTimeout(function () {
            $('.zoomContainer').remove();
            $('#zoom_01').elevateZoom({
                gallery: 'gallery_01',
                zoomWindowOffetx: 20,
                easing: true,
                scrollZoom: true,
                zoomWindowWidth: 400,
                zoomWindowHeight: 400,
                cursor: 'pointer',
                galleryActiveClass: 'active'
            });
        }, 300);
    }

</script>

<?php include __DIR__ . '/partials/footer.php'; ?>
