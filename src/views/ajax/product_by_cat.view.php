<?php if (!empty($this->products_in_cat)): ?>
    <div>
        <div class="module-content">
            <div class="ajax-carousel owl_product_ owl-carousel ajaxcol" data-nav="true" data-margin="0"
                 data-lg-items="5"
                 data-md-items="4" data-height="false" data-xs-items="2" data-xss-items="2" data-sm-items="3">
                <?php foreach ($this->products_in_cat as $item): ?>
                    <?php include(SITE_ROOT . '/src/views/partials/home/item-product.php'); ?>
                <?php endforeach; ?>
            </div>

            <p class="view-more-product">
                <a href="/product-cat?id=<?= $this->cat['id']; ?>" title="Xem tất cả">Xem tất cả</a>
            </p>
        </div>
    </div>
<?php else: ?>
    <div>
        <div class="fw bg-warning padding-15 noproduct">
            <div class="text-warning">
                Hiện tại chưa có sản phẩm nào trong danh mục này!...
            </div>
        </div>
    </div>
<?php endif; ?>


