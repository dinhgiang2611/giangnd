<nav class="main-menu <?php if (!$this->isIndex()) echo 'main-menu-coll'; ?>">
    <div class="container">
        <div class="menu-bar button-menu hidden-md hidden-lg">
            <a href="javascript:;">
                <i class="fa fa-align-justify"></i>
            </a>
        </div>
        <div class="menu-cate hidden-sm hidden-xs">
			<span class="title">
				<i class="fa fa-bars"></i> Danh mục sản phẩm
			</span>
            <?php
            $categories = $this->productCategories->getParents();
            if (!empty($categories)) {
                ?>
                <ul class="lv0">
                    <?php foreach ($categories as $category): ?>
                        <li>
                            <a class="nav-link" href="/product-cat?id=<?= $category['id']; ?>"
                               title="<?= $category['name']; ?>">
                                <?= $category['name']; ?>
                            </a>

                            <?php
                            $children = $this->productCategories->getChildren($category['id']);
                            if (!empty($children)):
                                ?>
                                <i class="fa fa-angle-right"></i>
                                <ul class="lv1">
                                    <?php foreach ($children as $child): ?>
                                        <li>
                                            <a href="/product-cat?id=<?= $child['id']; ?>"
                                               title="<?= $child['name']; ?>">
                                                <?= $child['name']; ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <?php
            }
            ?>
        </div>


        <ul class="level0 hidden-sm hidden-xs">
            <li class="nav-item active"><a class="nav-link" href="/" title="Trang chủ">Trang chủ</a></li>
            <li class="nav-item "><a class="nav-link" href="/about" title="Giới thiệu">Giới thiệu</a></li>
            <li class="nav-item  has-mega">
                <a href="#" class="arrow_nav nav-link" title="Sản phẩm">
                    Sản phẩm <i class="fa fa-caret-down hidden-sm"></i>
                </a>
                <?php
                if (!empty($categories)) {
                    ?>
                    <i class="fa fa-angle-down hidden-md hidden-lg"></i>
                    <ul class="level1 mega">
                        <?php foreach ($categories as $category): ?>
                            <li class="level2 parent item">
                                <a href="/product-cat?id=<?= $category['id']; ?>"
                                   title="<?= $category['name']; ?>">
                                    <span><?= $category['name']; ?></span>
                                </a>

                                <?php
                                $children = $this->productCategories->getChildren($category['id']);
                                if (!empty($children)):
                                    ?>
                                    <ul class="level2">
                                        <?php foreach ($children as $child): ?>
                                            <li class="level3">
                                                <a href="/product-cat?id=<?= $child['id']; ?>"
                                                   title="<?= $child['name']; ?>"><span><?= $child['name']; ?></span></a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <?php
                }
                ?>

            </li>

            <li class="nav-item "><a class="nav-link" href="/news" title="Tin tức">Tin tức</a></li>
            <li class="nav-item "><a class="nav-link" href="/contact" title="Liên hệ">Liên hệ</a></li>
        </ul>

        <form class="search-bar" action="?page=search" method="get" role="search">
            <input type="search" name="query" value="" placeholder="Tìm kiếm... " autocomplete="off">
            <button class="btn icon-fallback-text">
                <img src="images/icon-search.png"
                     alt="Tìm kiếm" width="18">
            </button>
        </form>
    </div>
</nav>