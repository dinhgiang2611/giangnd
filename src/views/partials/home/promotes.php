<?php
if (!empty($this->promotes)):
    ?>
    <section class="awe-section-3">
        <section class="section-product section-product-1">
            <div class="container">
                <div class="row">

                    <div class="col-md-3 col-sm-4">
                        <h2><a href="" title="SẢN PHẨM KHUYẾN MẠI"><span>SẢN PHẨM</span>KHUYẾN MẠI</a></h2>
                        <a class="banner hidden-xs" href="#" title="Sale Off 50%">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
                                 data-lazyload="images/banner_product.jpg?1577779848243"
                                 alt="Sale Off 50%"/>
                        </a>
                    </div>

                    <div class="col-md-9 col-sm-8 pad0">

                        <div class="owl-carousel" data-nav="true">

                            <?php foreach ($this->promotes as $item):?>
                                <?php include 'item-product.php'; ?>
                            <?php endforeach; ?>

                        </div>

                    </div>
                    <div class="col-md-3 col-sm-4 visible-xs">
                        <a class="banner visible-xs" href="#" title="Sale Off 50%">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
                                 data-lazyload="images/banner_product.jpg?1577779848243"
                                 alt="Sale Off 50%"/>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </section>
<?php
endif;