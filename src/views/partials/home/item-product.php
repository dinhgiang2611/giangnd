<div class="item">
    <div class="product-box product-item-main">
        <div class="details">
            <?php if (!empty($item['sale_price'])): ?>
                <?php $sale = ((float)$item['price'] - (float)$item['sale_price']) / $item['price'] * 100; ?>
                <div class="sale-flash">- <?= $sale; ?>%</div>
            <?php endif; ?>

            <div class="product-thumbnail">
                <a class="image_thumb" href="/product?id=<?= $item['id']; ?>"
                   title="<?= $item['name']; ?>">
                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
                         data-lazyload="<?= UPLOAD . $item['image']; ?>" alt="<?= $item['name']; ?>">
                </a>
                <a href="/product?id=<?= $item['id']; ?>"
                   class="btn-white btn_view btn right-to quick-view hidden-sm hidden-xs"
                   title="Xem nhanh">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </a>
            </div>

            <div class="product-info product-bottom">
                <h3 class="product-name">
                    <a href="/product?id=<?= $item['id']; ?>"
                       title="<?= $item['name']; ?>"><?= $item['name']; ?></a>
                </h3>
                <div class="product-item-price price-box">
                    <span class="product-price special-price"><?= $this->formatPrice($item['sale_price']); ?></span>
                    <span class="product-price old-price"><?= $this->formatPrice($item['price']); ?></span>
                </div>
            </div>
        </div>


        <div class="product-action clearfix hidden-xs">

            <form action="ajax/add-cart.php" method="post"
                  class="variants form-nut-grid" enctype="multipart/form-data">

                <div class="group_action">

                    <input class="hidden" type="hidden" name="variantId"
                           value="<?= $item['id']; ?>"/>
                    <button class="btn-cart btn btn-gray left-to" title="Chọn sản phẩm"
                            type="button"
                            onclick="window.location.href='/product?id=<?= $item['id']; ?>'">
                        Chọn sản phẩm
                    </button>

                </div>
            </form>

        </div>
    </div>
</div>