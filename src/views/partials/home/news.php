<section class="awe-section-7">
    <section class="section_blogs">
        <div class="container">
            <h2><a href="/news" title="GÓC TIN TỨC">GÓC TIN TỨC</a></h2>

            <?php if (!empty($this->h_news)): ?>
                <div class="list-blogs">
                    <div class="owl-carousel owl-blog-index"
                         data-nav="true"
                         data-loop="true"
                         data-lg-items="3"
                         data-md-items="3"
                         data-height="false"
                         data-xs-items="1"
                         data-sm-items="2"
                         data-margin="30">

                        <?php foreach ($this->h_news as $item): ?>
                            <div class="blog-inner">
                                <?php if (!empty($item['image'])): ?>
                                    <div class="thumbnail-article">
                                        <a href="/new?id=<?= $item['id']; ?>"
                                           title="<?= $item['title']; ?>" class="blog-img">

                                            <picture>
                                                <source media="(max-width: 480px)"
                                                        srcset="images/loading.svg?1577779848243"
                                                        data-lazyload3="<?= UPLOAD . $item['image']; ?>">
                                                <source media="(min-width: 481px) and (max-width: 767px)"
                                                        srcset="<?= UPLOAD . $item['image']; ?>">
                                                <source media="(min-width: 768px) and (max-width: 1023px)"
                                                        srcset="<?= UPLOAD . $item['image']; ?>">
                                                <source media="(min-width: 1024px) and (max-width: 1199px)"
                                                        srcset="<?= UPLOAD . $item['image']; ?>">
                                                <source media="(min-width: 1200px)"
                                                        srcset="<?= UPLOAD . $item['image']; ?>">
                                                <img src="images/loading.svg?1577779848243"
                                                     data-lazyload="<?= UPLOAD . $item['image']; ?>"
                                                     style="max-width:100%;" class="img-responsive"
                                                     alt="<?= $item['title']; ?>">
                                            </picture>

                                        </a>
                                    </div>
                                <?php endif; ?>

                                <div class="content__">
                                    <h3>
                                        <a title="<?= $item['title']; ?>" href="/new?id=<?= $item['id']; ?>">
                                            <?= $item['title']; ?>
                                        </a>
                                    </h3>
                                    <p class="meta-article">
                                        <i class="fa fa-calendar"></i> <?= date('d-m-Y', strtotime($item['date'])); ?>
                                    </p>
                                    <p class="meta-content">
                                        <?= $this->truncate($item['content']); ?>
                                    </p>
                                    <a class="view-more" href="/new?id=<?= $item['id']; ?>"
                                       title="Đọc tiếp">ĐỌC TIẾP <i class="fa fa-caret-right"></i>
                                    </a>
                                </div>
                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>
</section>