<section class="awe-section-1">
    <section class="section-slider">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="home-slider owl-carousel" data-play='true' data-loop='true' data-lg-items='1'
                         data-md-items='1' data-sm-items='1' data-xs-items="1" data-margin='0' data-dot="false"
                         data-nav="true">


                        <div class="item">
                            <a href="#" class="clearfix" title="">
                                <picture>
                                    <source media="(max-width: 480px)"
                                            data-lazyload3="https://bizweb.dktcdn.net/thumb/large/100/366/480/themes/736376/assets/slider_1.jpg?1577779848243"
                                            srcset="images/loading.svg?1577779848243">
                                    <img src="images/slider_1.jpg?1577779848243"
                                         style="max-width:100%;" class="img-responsive" alt="">
                                </picture>
                            </a>
                        </div>


                        <div class="item">
                            <a href="#" class="clearfix" title="">
                                <picture>
                                    <source media="(max-width: 480px)"
                                            data-lazyload3="https://bizweb.dktcdn.net/thumb/large/100/366/480/themes/736376/assets/slider_2.jpg?1577779848243"
                                            srcset="images/loading.svg?1577779848243">
                                    <img src="images/slider_2.jpg?1577779848243"
                                         style="max-width:100%;" class="img-responsive" alt="">
                                </picture>
                            </a>
                        </div>


                    </div><!-- /.products -->
                </div>
                <div class="col-md-3 padleft0">
                    <div class="row">


                        <div class="hidden-xs col-sm-6 col-md-12 col-lg-12 index-banner">
                            <div class="item banner-item">
                                <a href="#" title="">
                                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
                                         data-lazyload="images/slide_banner_1_image.jpg?1577779848243"
                                         alt="">
                                </a>
                            </div>
                        </div>


                        <div class="hidden-xs col-sm-6 col-md-12 col-lg-12 index-banner">
                            <div class="item banner-item">
                                <a href="#" title="">
                                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
                                         data-lazyload="images/slide_banner_2_image.jpg?1577779848243"
                                         alt="">
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</section>