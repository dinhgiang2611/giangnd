<section class="awe-section-4">
    <section class="section-product section-product-tab ajax-product">
        <div class="col-product-wrap">
            <div class="e-tabs2 not-dqtab ajax-tab-1" data-section="ajax-tab-1">
                <div class="content">
                    <div class="module-title">
                        <div class="container">
                            <div class="tabs-title-ajax">
                                <h2 class="title-mobile hidden-lg hidden-md">
                                    <a href="#" title="Danh mục sản phẩm">Danh mục sản phẩm</a>
                                </h2>

                                <ul class="tabs tabs-title tab-mobile clearfix hidden-md hidden-lg">
                                    <li class="prev"><i class="fa fa-angle-left"></i></li>
                                    <li class="tab-link tab-title  hidden-md hidden-lg current tab-titlexs"
                                        data-tab="tab-1">
                                        <span><?= $this->firstCategory['name']; ?></span>
                                    </li>
                                    <li class="next"><i class="fa fa-angle-right"></i></li>
                                </ul>

                                <div class="title-desktop hidden-sm hidden-xs">
                                    <h2>
                                        <a href="?page=collections" title="Danh mục sản phẩm">Danh mục sản phẩm</a>
                                    </h2>
                                    <?php
                                    if (!empty($this->getCatsWithLimit)):
                                        ?>
                                        <ul class="tabs tabs-title ajax clearfix hidden-xs">
                                            <?php $j = 1;
                                            foreach ($this->getCatsWithLimit as $cat): ?>
                                                <li class="tab-link <?= ($j === 1) ? 'has-content' : ''; ?>"
                                                    data-tab="tab-<?= $j; ?>"
                                                    data-id="<?= $cat['id']; ?>">
                                                    <span><?= $cat['name']; ?></span>
                                                </li>
                                                <?php $j++; endforeach; ?>
                                        </ul>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="row">

                            <?php
                            if (!empty($this->getCatsWithLimit)):
                                $k = 1;
                                foreach ($this->getCatsWithLimit as $cat):
                                    ?>

                                    <div class="tab-<?= $k; ?> tab-content">

                                        <?php if ($k === 1) : ?>

                                            <div class="owl_product_ owl-carousel ajaxcol" data-nav="true"
                                                 data-margin="0"
                                                 data-lg-items="5" data-md-items="4" data-height="false"
                                                 data-xs-items="2"
                                                 data-xss-items="2" data-sm-items="3">

                                                <?php
                                                $products = $this->products->getByCategoriesRelation($cat['id'], 4);
                                                if (!empty($products)):
                                                    foreach ($products as $item):
                                                        ?>
                                                        <?php include 'item-product.php'; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>

                                            <p class="view-more-product">
                                                <a href="/product-cat?id=<?= $cat['id']; ?>" title="Xem tất cả">Xem
                                                    tất cả</a>
                                            </p>

                                        <?php endif; ?>

                                    </div>

                                    <?php
                                    $k++;
                                endforeach;
                            endif;
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>