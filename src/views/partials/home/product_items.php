<section class="awe-section-6">
    <section class="section-twocol">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h2><a href="/product-cat?id=1">ĐIỀU HÒA</a></h2>

                    <div class="list-product-mini">
                        <?php
                        if (!empty($this->products_dh)):
                            foreach ($this->products_dh as $item):
                                ?>
                                <div class="product-box-mini">
                                    <div class="product-thumbnail">
                                        <a class="image_thumb" href="/product?id=<?= $item['id']; ?>"
                                           title="<?= $item['name']; ?>">
                                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
                                                 data-lazyload="<?= UPLOAD . $item['image']; ?>"
                                                 alt="<?= $item['name']; ?>">
                                        </a>
                                    </div>
                                    <div class="product-info product-bottom">
                                        <h3 class="product-name">
                                            <a href="/product?id=<?= $item['id']; ?>"
                                               title="<?= $item['name']; ?>">
                                                <?= $item['name']; ?>
                                            </a>
                                        </h3>

                                        <div class="product-item-price price-box">
                                            <span class="product-price special-price"><?= $this->formatPrice($item['sale_price']); ?></span>
                                            <span class="product-price old-price"><?= $this->formatPrice($item['price']); ?></span>
                                        </div>

                                        <form action="/cart/add" method="post" class="variants form-nut-grid"
                                              data-id="product-actions-<?= $item['id']; ?>" enctype="multipart/form-data">
                                            <div class="group_action">

                                                <input class="hidden" type="hidden" name="variantId" value="28391080"/>
                                                <button class="btn-cart btn btn-gray  left-to" title="Chọn sản phẩm"
                                                        type="button"
                                                        onclick="window.location.href='/product?id=<?= $item['id']; ?>'">
                                                    Chọn sản phẩm
                                                </button>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            <?php
                            endforeach;
                        endif;
                        ?>

                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <h2><a href="/product-cat?id=10" title="MÁY GIẶT">MÁY GIẶT</a></h2>
                    <div class="list-product-mini">
                        <?php
                        if (!empty($this->products_mg)):
                            foreach ($this->products_mg as $item):
                                ?>
                                <div class="product-box-mini">
                                    <div class="product-thumbnail">
                                        <a class="image_thumb" href="/product?id=<?= $item['id']; ?>"
                                           title="<?= $item['name']; ?>">
                                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
                                                 data-lazyload="<?= UPLOAD . $item['image']; ?>"
                                                 alt="<?= $item['name']; ?>">
                                        </a>
                                    </div>
                                    <div class="product-info product-bottom">
                                        <h3 class="product-name">
                                            <a href="/product?id=<?= $item['id']; ?>"
                                               title="<?= $item['name']; ?>">
                                                <?= $item['name']; ?>
                                            </a>
                                        </h3>

                                        <div class="product-item-price price-box">
                                            <span class="product-price special-price"><?= $this->formatPrice($item['sale_price']); ?></span>
                                            <span class="product-price old-price"><?= $this->formatPrice($item['price']); ?></span>
                                        </div>

                                        <form action="/cart/add" method="post" class="variants form-nut-grid"
                                              data-id="product-actions-<?= $item['id']; ?>" enctype="multipart/form-data">
                                            <div class="group_action">

                                                <input class="hidden" type="hidden" name="variantId" value="28391080"/>
                                                <button class="btn-cart btn btn-gray  left-to" title="Chọn sản phẩm"
                                                        type="button"
                                                        onclick="window.location.href='/product?id=<?= $item['id']; ?>'">
                                                    Chọn sản phẩm
                                                </button>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            <?php
                            endforeach;
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>