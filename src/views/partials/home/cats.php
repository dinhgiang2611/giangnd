<?php
if (!empty($this->categories)) {
    ?>
    <section class="awe-section-2">
        <section class="section-cate">
            <div class="container">
                <div class="cate-slider owl-carousel" data-lg-items='7' data-md-items='6' data-sm-items='4'
                     data-xs-items="2" data-margin='30' data-nav="true">

                    <?php $i = 1;
                    foreach ($this->categories as $category): ?>

                        <a href="/product-cat?id=<?= $category['id']; ?>" class="cate-<?= $i; ?> item"
                           title="<?= $category['name']; ?>"><?= $category['name']; ?></a>

                        <?php $i++;
                    endforeach; ?>

                </div>
            </div>
        </section>
    </section>
    <?php
}