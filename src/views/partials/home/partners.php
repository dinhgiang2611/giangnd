<section class="awe-section-5">
    <section class="section-brands">
        <div class="container">
            <div class="owl-carousel owl-theme nav-enable nav-center" data-lg-items="5" data-md-items="4"
                 data-sm-items="4" data-xs-items="3" data-xxs-items="2" data-margin="10" data-play="true"
                 data-loop="true">


                <div class="item">
                    <a href="#" title="DeWALT">
                        <img class="img-responsive" alt="dewalt"
                             src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
                             data-lazyload="https://bizweb.dktcdn.net/thumb/compact/100/366/480/themes/736376/assets/brand_1_image.png?1577779848243"/>
                    </a>
                </div>


                <div class="item">
                    <a href="#" title="Stanley">
                        <img class="img-responsive" alt="stanley"
                             src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
                             data-lazyload="https://bizweb.dktcdn.net/thumb/compact/100/366/480/themes/736376/assets/brand_2_image.png?1577779848243"/>
                    </a>
                </div>


                <div class="item">
                    <a href="#" title="Makita">
                        <img class="img-responsive" alt="makita"
                             src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
                             data-lazyload="https://bizweb.dktcdn.net/thumb/compact/100/366/480/themes/736376/assets/brand_3_image.png?1577779848243"/>
                    </a>
                </div>


                <div class="item">
                    <a href="#" title="Bosch">
                        <img class="img-responsive" alt="bosch"
                             src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
                             data-lazyload="https://bizweb.dktcdn.net/thumb/compact/100/366/480/themes/736376/assets/brand_4_image.png?1577779848243"/>
                    </a>
                </div>


                <div class="item">
                    <a href="#" title="Honda">
                        <img class="img-responsive" alt="honda"
                             src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
                             data-lazyload="https://bizweb.dktcdn.net/thumb/compact/100/366/480/themes/736376/assets/brand_5_image.png?1577779848243"/>
                    </a>
                </div>


            </div>
        </div>
    </section>
</section>