<div class="topbar hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xs-5 topbar-left">
                <span class="hidden-xs">Chào mừng bạn đến ShopNhatBai.vn | Thế giới hàng Nhật bãi</span>
                <a class="visible-xs" href="tel:<?= str_replace(" ", "", PHONE_NUMBER); ?>">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <?= PHONE_NUMBER; ?>
                </a>
            </div>
            <div class="col-sm-6 col-xs-7 topbar-right">
                <a href="/login" title="Đăng nhập">Đăng nhập</a>
                <span>&nbsp;|&nbsp;</span>
                <a href="/register" title="Đăng ký">Đăng ký</a>
            </div>
        </div>
    </div>
</div>