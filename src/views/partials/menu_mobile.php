<div class="menu_mobile max_991 hidden-md hidden-lg">
    <div class="account">
        <i class="fa fa-user"></i>

        <a href="/login" title="Đăng nhập">Đăng nhập</a>
        <span>&nbsp;|&nbsp;</span>
        <a href="/register" title="Đăng ký">Đăng ký</a>

        <i class="fa fa-arrow-left"></i>
    </div>

    <ul class="ul_collections">
        <li class="level0 level-top parent">
            <a href="/" title="Trang chủ">Trang chủ</a>
        </li>

        <li class="level0 level-top parent">
            <a href="/about" title="Giới thiệu">Giới thiệu</a>
        </li>

        <li class="level0 level-top parent">
            <a href="#" title="Sản phẩm">Sản phẩm</a>

            <?php
            $categories = $this->productCategories->getParents();
            if (!empty($categories)) {
                ?>
                <i class="fa fa-angle-down"></i>
                <ul class="level0" style="display:none;">
                    <?php foreach ($categories as $category): ?>
                        <li class="level1 ">
                            <a href="/product-cat?id=<?= $category['id']; ?>" title="<?= $category['name']; ?>">
                                <span><?= $category['name']; ?></span>
                            </a>

                            <?php
                            $children = $this->productCategories->getChildren($category['id']);
                            if (!empty($children)):
                                ?>
                                <i class="fa fa-angle-down"></i>
                                <ul class="level1" style="display:none;">

                                    <?php foreach ($children as $child): ?>
                                        <li class="level3 ">
                                            <a href="/product-cat?id=<?= $child['id']; ?>"
                                               title="<?= $child['name']; ?>">
                                                <span><?= $child['name']; ?></span>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>

                                </ul>
                            <?php endif; ?>

                        </li>
                    <?php endforeach; ?>
                </ul>
                <?php
            }
            ?>
        </li>

        <li class="level0 level-top parent">
            <a href="/news" title="Tin tức">Tin tức</a>
        </li>

        <li class="level0 level-top parent">
            <a href="/contact" title="Liên hệ">Liên hệ</a>
        </li>

    </ul>
</div>