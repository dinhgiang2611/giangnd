<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hidden-sm hidden-xs side-product">
    <div class="product-policy border-solid">
        <p>Trong 1 - 2 Giờ làm việc không bao gồm chủ nhật và ngày lễ</p>
        <p>Đổi trả sản phẩm trong vòng 7 ngày nếu có lỗi từ nhà sản xuất</p>
        <p>
            Hotline tư vấn<a href="tel:<?= str_replace(' ', '', PHONE_NUMBER); ?>"><?= PHONE_NUMBER; ?></a>
        </p>
    </div>

    <div class="product-news border-solid">
        <h2>TÌM HIỂU THÊM</h2>
        <?php if (!empty($this->news)): ?>
            <ul>
                <?php foreach ($this->news as $item): ?>
                    <li>
                        <i class="fa fa-caret-right"></i>
                        <a href="/new?id=<?= $item['id']; ?>" title="<?= $item['title']; ?>" target="_blank">
                            <?= $item['title']; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>