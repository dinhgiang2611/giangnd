<div class="price-box" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
    <span class="special-price">
        <span class="price product-price"><?= $this->formatPrice($this->product['sale_price']); ?></span>
         <?php if (!empty($this->product['sale_price'])): ?>
             <?php $sale = ((float)$this->product['price'] - (float)$this->product['sale_price']) / $this->product['price'] * 100; ?>
             <span class="sale-tag">-<?= $sale; ?>%</span>
         <?php endif; ?>
    </span>
    <span class="old-price">
        <del class="price product-price-old" itemprop="priceSpecification">
            <?= $this->formatPrice($this->product['price']); ?>
        </del>
        <meta itemprop="price" content="<?= $this->product['sale_price']; ?>">
        <meta itemprop="priceCurrency" content="VND">
    </span>
    <link itemprop="availability" href="http://schema.org/InStock">
    <meta itemprop="url" content="/product?id=<?= $this->product['id']; ?>">
</div>