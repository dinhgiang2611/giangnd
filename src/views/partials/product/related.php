<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 related_module margin-bottom-30">
    <h2 class="title_module_child">
        <a href="#" title="Sản phẩm cùng loại">
            Sản phẩm cùng loại
        </a>
    </h2>

    <?php if (!empty($this->related)): ?>
        <div class="row">
            <div class="wrap_owl">

                <div class="owl-carousel owl-related"
                     data-nav="true"
                     data-lg-items="5"
                     data-md-items="4"
                     data-height="true"
                     data-xs-items="2"
                     data-sm-items="3"
                     data-margin="0">

                    <?php foreach ($this->related as $item): ?>
                        <?php include SITE_ROOT  . '/src/views/partials/home/item-product.php'; ?>
                    <?php endforeach; ?>

                </div>

            </div>
        </div>
    <?php endif; ?>
</div>