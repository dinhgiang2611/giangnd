<div class="product-detail-left product-images col-xs-12 col-sm-6 col-md-5 col-lg-5">
    <div class="rows">
        <div class="relative product-image-block no-thum">
            <div class="large-image">
                <a href="<?= UPLOAD . $this->product['image']; ?>"
                   class="large_image_url checkurl" data-rel="prettyPhoto[product-gallery]">

                    <img id="zoom_01" class="img-responsive center-block"
                         src="<?= UPLOAD . $this->product['image']; ?>"
                         alt="<?= $this->product['name']; ?>">
                </a>

                <?php
                $product_images = $this->products->getProductImages($this->product['id']);
                if (!empty($product_images)) {
                    ?>

                    <div class="offset_nop hidden">

                        <?php
                        foreach ($product_images as $image) {
                            ?>
                            <div class="item">
                                <a class="check_data_img"
                                   href="<?= UPLOAD . $image['image']; ?>"
                                   data-image="<?= UPLOAD . $image['image']; ?>"
                                   data-zoom-image="<?= UPLOAD . $image['image']; ?>"
                                   data-rel="prettyPhoto[product-gallery]"></a>
                            </div>
                            <?php
                        }
                        ?>

                    </div>
                    <?php
                }
                ?>

            </div>

            <?php
            if (!empty($product_images)) {
                echo '<div id="gallery_01" class="owl-carousel owl-theme" data-lg-items="4" data-md-items="4" data-sm-items="4" data-xs-items="3" data-margin="10" data-nav="true">';
                ?>
                <div class="item">
                    <a class="border-radius-10" href="javascript:void(0);"
                       data-image="<?= UPLOAD . $this->product['image']; ?>"
                       data-zoom-image="<?= UPLOAD . $this->product['image']; ?>">
                        <img src="<?= UPLOAD . $this->product['image']; ?>"
                             class="img-responsive" alt="<?= $this->product['name']; ?>">
                    </a>
                </div>
                <?php
                foreach ($product_images as $image) {
                    ?>
                    <div class="item">
                        <a class="border-radius-10" href="javascript:void(0);"
                           data-image="<?= UPLOAD . $image['image']; ?>"
                           data-zoom-image="<?= UPLOAD . $image['image']; ?>">
                            <img src="<?= UPLOAD . $image['image']; ?>"
                                 class="img-responsive"
                                 alt="<?= $this->product['name']; ?>">
                        </a>
                    </div>
                    <?php
                }
                echo '</div>';
            }
            ?>

        </div>
    </div>
</div>