<div class="form-product">

    <div class="box-variant clearfix ">
        <input type="hidden" name="variantId" value="<?= $this->product['id']; ?>">
    </div>

    <div class="form-group form-buy-product">
        <div class="form_button_details">
            <span class="not_bg">Số lượng:</span>
            <div class="custom input_number_product custom-btn-number">
                <button class="btn_num num_2 button button_qty"
                        onclick="var result = document.getElementById('qtym'); var qtypro = result.value; if( !isNaN( qtypro )) result.value++;return false;"
                        type="button">+
                </button>
                <button class="btn_num num_1 button button_qty"
                        onclick="var result = document.getElementById('qtym'); var qtypro = result.value; if( !isNaN( qtypro ) &amp;&amp; qtypro > 1 ) result.value--;return false;"
                        type="button">-
                </button>
                <input type="text" maxlength="2" id="qtym" name="quantity" value="1"
                       class="form-control prd_quantity"
                       onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;"
                       onchange="if(this.value == 0)this.value=1;">
            </div>
        </div>

        <button type="submit"
                class="btn btn-lg  btn-cart button_cart_buy_enable add_to_cart btn_buy">
            <span>Thêm vào giỏ</span>
        </button>

        <a href="javascript:;" data-id="<?= $this->product['id']; ?>" data-qty="1"
           class="buy-now">
            MUA NGAY
        </a>
    </div>

    <div class="hotline">
        <b>Tư vấn</b>
        <img src="images/product_hotline.png" alt="hotline">
        <a href="tel:<?= str_replace(' ', '', PHONE_NUMBER); ?>" title="hotline">
            <?= PHONE_NUMBER; ?>
        </a>
    </div>

</div>