<div class="col-xs-12 col-sm-12 col-lg-3 col-md-3">
    <div class="product-hot border-solid">
        <h2>Ðược mua nhiều</h2>

        <div class="list">
            <?php if (!empty($this->best_sellers)): ?>
                <?php foreach ($this->best_sellers as $item): ?>
                    <div class="product-box product-box-mini">
                        <div class="product-thumbnail">
                            <a class="image_thumb" href="/product?id=<?= $item['id']; ?>" title="<?= $item['name']; ?>">
                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
                                     data-lazyload="<?= UPLOAD . $item['image']; ?>"
                                     alt="<?= $item['name']; ?>">
                            </a>
                        </div>

                        <div class="product-info product-bottom">
                            <h3 class="product-name">
                                <a href="/product?id=<?= $item['id']; ?>" title="<?= $item['name']; ?>"><?= $item['name']; ?></a>
                            </h3>
                            <div class="product-item-price price-box">

                                <span class="special-price">
                                    <span class="price product-price"><?= $this->formatPrice($item['sale_price']); ?></span>
                                </span>

                                <span class="product-item-price-sale old-price">
                                    <span class="compare-price price product-price-old"><?= $this->formatPrice($item['price']); ?></span>
                                </span>

                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>