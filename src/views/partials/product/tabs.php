<!-- Tab -->
<div class="tab_width_full">
    <div class="row xs-margin-top-15">
        <div id="tab_ord" class="col-xs-12 col-sm-12 col-lg-9 col-md-9">
            <div class="product-tab e-tabs not-dqtab">
                <span class="border-dashed-tab"></span>
                <ul class="tabs tabs-title clearfix">
                    <li class="tab-link current" data-tab="tab-1">Mô tả</li>
                    <li class="tab-link" data-tab="tab-2">Hướng dẫn mua hàng</li>
                </ul>
                <div class="tab-float">

                    <div id="tab-1" class="tab-content content_extab current">
                        <div class="rte">

                            <?php echo stripcslashes($this->product['content']); ?>

                        </div>
                    </div>


                    <div id="tab-2" class="tab-content content_extab">
                        <div class="rte">

                            <p><strong>Bước 1:</strong>&nbsp;Truy cập website và lựa chọn sản
                                phẩm cần mua để mua hàng</p>
                            <p><strong>Bước 2:</strong>&nbsp;Click và sản phẩm muốn mua, màn
                                hình hiển thị ra pop up với các lựa chọn sau</p>
                            <p>Nếu bạn muốn tiếp tục mua hàng: Bấm vào phần tiếp tục mua hàng để
                                lựa chọn thêm sản phẩm vào giỏ hàng</p>
                            <p>Nếu bạn muốn xem giỏ hàng để cập nhật sản phẩm: Bấm vào xem giỏ
                                hàng</p>
                            <p>Nếu bạn muốn đặt hàng và thanh toán cho sản phẩm này vui lòng bấm
                                vào: Đặt hàng và thanh toán</p>
                            <p><strong>Bước 3:</strong>&nbsp;Lựa chọn thông tin tài khoản thanh
                                toán</p>
                            <p>Nếu bạn đã có tài khoản vui lòng nhập thông tin tên đăng nhập là
                                email và mật khẩu vào mục đã có tài khoản trên hệ thống</p>
                            <p>Nếu bạn chưa có tài khoản và muốn đăng ký tài khoản vui lòng điền
                                các thông tin cá nhân để tiếp tục đăng ký tài khoản. Khi có tài
                                khoản bạn sẽ dễ dàng theo dõi được đơn hàng của mình</p>
                            <p>Nếu bạn muốn mua hàng mà không cần tài khoản vui lòng nhấp chuột
                                vào mục đặt hàng không cần tài khoản</p>
                            <p><strong>Bước 4:</strong>&nbsp;Điền các thông tin của bạn để nhận
                                đơn hàng, lựa chọn hình thức thanh toán và vận chuyển cho đơn
                                hàng của mình</p>
                            <p><strong>Bước 5:</strong>&nbsp;Xem lại thông tin đặt hàng, điền
                                chú thích và gửi đơn hàng</p>
                            <p>Sau khi nhận được đơn hàng bạn gửi chúng tôi sẽ liên hệ bằng cách
                                gọi điện lại để xác nhận lại đơn hàng và địa chỉ của bạn.</p>
                            <p>Trân trọng cảm ơn.</p>


                        </div>
                    </div>

                </div>
            </div>
        </div>

        <?php include 'right-tab.php'; ?>

    </div>
</div>
<!-- Endtab -->