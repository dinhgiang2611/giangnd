<section class="bread-crumb">
    <span class="crumb-border"></span>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 a-left">
                <ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <li class="home">
                        <a itemprop="url" href="/"><span itemprop="title">Trang chủ</span></a>
                        <span class="mr_lr"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>