<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $this->pageTitle ? $this->pageTitle . ' &ndash; ' . SITE_NAME : SITE_NAME; ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="<?= $this->getStylesheet('main'); ?>?<?= time(); ?>" rel="stylesheet">
    <link href="<?= $this->getImage('favicon.png'); ?>" rel="icon">
    <script src="//code.jquery.com/jquery-2.2.4.min.js" type="text/javascript"></script>
</head>

<body>
<div class="backdrop__body-backdrop___1rvky"></div>
<div class="page-body">
    <div class="hidden-md hidden-lg opacity_menu"></div>
    <div class="opacity_filter"></div>
    <div class="body_opactiy"></div>
    <div class="op_login"></div>

    <header class="header topbar_ect">
        <?php include __DIR__ . '/top_bar.php'; ?>
        <?php include __DIR__ . '/main_header.php'; ?>
        <?php include __DIR__ . '/main_menu.php'; ?>
    </header>

    <?php include __DIR__ . '/menu_mobile.php'; ?>
