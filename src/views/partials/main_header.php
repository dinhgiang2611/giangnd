<div class="main-header">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-8 logo">
                <a href="/" title="<?= SITE_NAME; ?>">
                    <img src="images/logo.png" alt="<?= SITE_NAME; ?>"/>
                </a>
            </div>

            <div class="col-md-9 col-xs-4">
                <div class="col-md-9 policy-header hidden-sm hidden-xs">
                    <div class="item">
                        <img src="images/contact_1_icon.png"
                             alt="Địa chỉ"/>
                        <span>Địa chỉ</span>
                        <b><a href="?page=lien-he" title="">
                                1022 Quang Trung, HĐ, HN
                            </a></b>
                    </div>
                    <div class="item">
                        <img src="images/contact_2_icon.png"
                             alt="Hỗ trợ mua hàng"/>
                        <span>Hỗ trợ mua hàng</span>
                        <b><a href="tel:<?= str_replace(" ", "", PHONE_NUMBER); ?>"
                              title="<?= PHONE_NUMBER; ?>"><?= PHONE_NUMBER; ?></a></b>
                    </div>
                    <div class="item hidden-md">
                        <img src="images/contact_3_icon.png"
                             alt="Hỗ trợ bảo hành"/>
                        <span>Hỗ trợ bảo hành</span>
                        <b><a href="tel:<?= str_replace(" ", "", PHONE_NUMBER); ?>"
                              title="<?= PHONE_NUMBER; ?>"><?= PHONE_NUMBER; ?></a></b>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12 cart-header">
                    <div class="top-cart-contain f-right">
                        <div class="mini-cart text-xs-center">
                            <a href="?page=cart" title="Giỏ hàng" class="cart-link">
                                <img src="images/icon-cart.png"
                                     alt="Giỏ hàng">
                                <i class="cartCount count_item_pr"></i>
                                <span class="hidden-xs">Giỏ hàng</span>
                                <b class="hidden-xs">(<span class="cartCount">0</span>) sản phẩm</b>
                            </a>
                            <div class="top-cart-content">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>