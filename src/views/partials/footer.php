<footer class="footer">
    <div class="topfoter">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 footer-infor">

                    <a href="/" class="logo-footer" title="OH! Dụng cụ cơ khí">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
                             data-lazyload="images/logo_footer.png?1577779848243"
                             alt="OH! Dụng cụ cơ khí">
                    </a>


                    <p><b>Kênh thông tin của chúng tôi</b></p>
                    <ul class="social">

                        <li><a href="https://www.facebook.com/" title="facebook" class="facebook"><i
                                        class="fa fa-facebook"></i></a></li>


                        <li><a href="https://twitter.com/" title="twitter" class="twitter"><i
                                        class="fa fa-twitter"></i></a></li>


                        <li><a href="https://www.instagram.com/" title="instagram" class="instagram"><i
                                        class="fa fa-instagram"></i></a></li>


                        <li><a href="https://www.youtube.com/" title="youtube" class="youtube"><i
                                        class="fa fa-youtube"></i></a></li>

                    </ul>

                    <p><b>Chấp nhận thanh toán</b></p>
                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
                         data-lazyload="images/payment.png?1577779848243"
                         alt="Thanh toán"/>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 footer-click">
                    <h4 class="cliked">THÔNG TIN</h4>
                    <ul class="toggle-mn list-menu" style="display:none;">

                        <li><a class="ef" href="/about" title="Giới thiệu">Giới thiệu</a></li>

                        <li><a class="ef" href="/products" title="Sản phẩm">Sản phẩm</a></li>

                        <li><a class="ef" href="/news" title="Tin tức">Tin tức</a></li>

                        <li><a class="ef" href="/contact" title="Liên hệ">Liên hệ</a></li>

                    </ul>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 footer-click">
                    <h4 class="cliked">CHÍNH SÁCH</h4>
                    <ul class="toggle-mn list-menu" style="display:none;">

                        <li><a class="ef" href="/policy" title="Chính sách bảo mật">Chính sách bảo mật</a></li>

                        <li><a class="ef" href="/delivery-policy" title="Chính sách giao hàng">Chính sách giao hàng</a>
                        </li>

                        <li><a class="ef" href="/payment-guide" title="Hướng dẫn thanh toán">Hướng dẫn thanh toán</a>
                        </li>

                        <li><a class="ef" href="/help" title="Trợ giúp">Trợ giúp</a></li>

                    </ul>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 footer-click footer-col-last">
                    <h4 class="clikedfalse">THÔNG TIN LIÊN HỆ</h4>
                    <div class="classic-text-widget info-footer">

                        <p><i class="fa fa-map-marker"></i>1022 Quang Trung, Hà Đông, Hà Nội</p>
                        <p><i class="fa fa-phone"></i>
                            <a href="tel:<?= str_replace(" ", "", PHONE_NUMBER); ?>"
                               title="<?= PHONE_NUMBER ?>"> <?= PHONE_NUMBER ?></a>
                        </p>
                        <p>
                            <i class="fa fa-envelope"></i>
                            <a href="mailto:info@shopnhatbai.vn" title="info@shopnhatbai.vn">info@shopnhatbai.vn</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-footer fot_copyright a-center">
        <div class="container">
            <div class="row row_footer">
                Bản quyền thuộc về <span class="banquyen">Shop Nhật Bãi</span>
            </div>
        </div>
    </div>

    <a href="#" id="back-to-top" class="back-to-top" title="Lên đầu trang"><i class="fa fa-angle-up"></i></a>

</footer>

<script src="<?php echo $this->getScript('plugin'); ?>"></script>
<script src="<?php echo $this->getScript('main'); ?>?<?= time(); ?>"></script>
</body>

</html>