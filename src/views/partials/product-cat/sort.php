<div class="sortPagiBar">
    <div class="row">
        <div class="col-xs-5 col-md-7 col-sm-6">
            <div class="hidden-xs">
                <div class="tt hidden">
                    <div id="ttfix" class="hidden-sm hidden-xs">
                        Hiển thị <span>1</span> - <span>6</span> trong tổng số <span>6</span> sản
                        phẩm
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-5 col-sm-6 text-xs-left text-sm-right">
            <div id="sort-by">
                <label class="left">Sắp xếp: </label>
                <ul>
                    <li><span class="val">Thứ tự</span>
                        <ul class="ul_2">
                            <li><a href="javascript:;" onclick="sortby('')">Mặc định</a></li>
                            <li><a href="javascript:;" onclick="sortby('alpha-asc')">A → Z</a></li>
                            <li><a href="javascript:;" onclick="sortby('alpha-desc')">Z → A</a></li>
                            <li><a href="javascript:;" onclick="sortby('price-asc')">Giá tăng
                                    dần</a></li>
                            <li><a href="javascript:;" onclick="sortby('price-desc')">Giá giảm
                                    dần</a></li>
                            <li><a href="javascript:;" onclick="sortby('created-desc')">Hàng mới
                                    nhất</a></li>
                            <li><a href="javascript:;" onclick="sortby('created-asc')">Hàng cũ
                                    nhất</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>