<div class="alert alert-warning fade in green-alert" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span
            class="sr-only">Close</span></button>
    Không có sản phẩm nào trong danh mục này.
</div>