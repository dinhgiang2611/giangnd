<section class="products-view products-view-grid margin-bottom-50 collection_reponsive">
    <?php if (!empty($this->product_list)): ?>
        <div class="row">
            <?php
            foreach ($this->product_list as $item) :
                ?>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 product-col">
                    <div class="item_product_main margin-bottom-20">
                        <?php include SITE_ROOT . '/src/views/partials/home/item-product.php'; ?>
                    </div>
                </div>
            <?php
            endforeach;
            ?>
        </div>

        <div class="text-center xs_padding col-lg-12 col-md-12 col-sm-12 col-xs-12">

        </div>
    <?php else: ?>
        <?php include 'empty.php'; ?>
    <?php endif; ?>

</section>