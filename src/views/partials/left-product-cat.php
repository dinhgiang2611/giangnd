<aside class="dqdt-sidebar sidebar left left-content col-xs-12 col-lg-3 col-md-3 col-sm-12  col-lg-pull-9 col-md-pull-9">

    <?php
    $categories = $this->productCategories->getParents();
    if (!empty($categories)) {
        ?>
        <aside class="aside-item collection-category margin-bottom-25">
            <h2 class="title_module_arrow margin-top-0">
                <i class="fa fa-bars"></i> Danh mục sản phẩm
            </h2>

            <div class="aside-content aside-cate-link-cls">
                <nav class="cate_padding nav-category navbar-toggleable-md">
                    <ul class="nav-ul nav navbar-pills">
                        <?php $i = 1;
                        $catIdRelations = $this->products->getCatIdsRelation($this->id);
                        foreach ($categories as $category): ?>
                            <li class="nav-item lv1 <?= in_array((int)$category['id'], $catIdRelations) ? 'active' : ''; ?>">
                                <a href="/product-cat?id=<?= $category['id']; ?>" class="nav-link"
                                   title="><?= $category['name']; ?>"><?= $category['name']; ?></a>

                                <?php
                                $children = $this->productCategories->getChildren($category['id']);
                                if (!empty($children)):
                                    ?>
                                    <i class="fa fa-caret-down"></i>

                                    <ul class="dropdown-menu">
                                        <?php foreach ($children as $child): ?>
                                            <li class="dropdown-submenu nav-item lv2 <?= ((int)$child['id'] === $this->id) ? 'active' : ''; ?>">
                                                <a class="nav-link" href="/product-cat?id=<?= $child['id']; ?>"
                                                   title="<?= $child['name']; ?>">
                                                    <?= $child['name']; ?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                            <?php $i++; endforeach; ?>
                    </ul>
                </nav>
            </div>
        </aside>
        <?php
    }
    ?>


    <div class="aside-filter">
        <div class="filter-container">
            <div class="filter-container__selected-filter" style="display: none;">
                <div class="aside-title">
                    <h2 class="title-head margin-top-0"><span>Bộ lọc</span></h2>
                </div>
                <div class="filter-container__selected-filter-header clearfix">
                                <span class="filter-container__selected-filter-header-title">
                                    <i class="fa fa-arrow-left hidden-sm-up"></i> Bạn chọn
                                </span>
                    <a href="javascript:void(0)" onclick="clearAllFiltered()"
                       class="filter-container__clear-all">Bỏ hết <i class="fa fa-angle-right"></i></a>
                </div>
                <div class="filter-container__selected-filter-list">
                    <ul></ul>
                </div>
            </div>


            <aside class="aside-item aside-filter filter-price">
                <h2 class="module-title">Mức giá</h2>
                <div class="aside-content filter-group">
                    <ul>
                        <li class="filter-item filter-item--check-box filter-item--green">
						<span>
							<label for="filter-duoi-100-000d">
								<input type="checkbox" id="filter-duoi-100-000d" onchange="toggleFilter(this);"
                                       data-group="Khoảng giá" data-field="sale_price" data-text="Dưới 100.000đ"
                                       value="(<100000)" data-operator="OR">
								<i class="fa"></i>
								Giá dưới 100.000đ
							</label>
						</span>
                        </li>


                        <li class="filter-item filter-item--check-box filter-item--green">
						<span>
							<label for="filter-100-000d-200-000d">
								<input type="checkbox" id="filter-100-000d-200-000d" onchange="toggleFilter(this)"
                                       data-group="Khoảng giá" data-field="sale_price" data-text="100.000đ - 200.000đ"
                                       value="(>=100000 AND <200000)" data-operator="OR">
								<i class="fa"></i>
								100.000đ - 200.000đ
							</label>
						</span>
                        </li>


                        <li class="filter-item filter-item--check-box filter-item--green">
						<span>
							<label for="filter-200-000d-300-000d">
								<input type="checkbox" id="filter-200-000d-300-000d" onchange="toggleFilter(this)"
                                       data-group="Khoảng giá" data-field="sale_price" data-text="200.000đ - 300.000đ"
                                       value="(>=200000 AND <300000)" data-operator="OR">
								<i class="fa"></i>
								200.000đ - 300.000đ
							</label>
						</span>
                        </li>


                        <li class="filter-item filter-item--check-box filter-item--green">
						<span>
							<label for="filter-300-000d-500-000d">
								<input type="checkbox" id="filter-300-000d-500-000d" onchange="toggleFilter(this)"
                                       data-group="Khoảng giá" data-field="sale_price" data-text="300.000đ - 500.000đ"
                                       value="(>=300000 AND <500000)" data-operator="OR">
								<i class="fa"></i>
								300.000đ - 500.000đ
							</label>
						</span>
                        </li>


                        <li class="filter-item filter-item--check-box filter-item--green">
						<span>
							<label for="filter-500-000d-1-000-000d">
								<input type="checkbox" id="filter-500-000d-1-000-000d" onchange="toggleFilter(this)"
                                       data-group="Khoảng giá" data-field="sale_price" data-text="500.000đ - 1.000.000đ"
                                       value="(>500000 AND <1000000)" data-operator="OR">
								<i class="fa"></i>
								500.000đ - 1.000.000đ
							</label>
						</span>
                        </li>
                        <li class="filter-item filter-item--check-box filter-item--green">
						<span>
							<label for="filter-tren1-000-000d">
								<input type="checkbox" id="filter-tren1-000-000d" onchange="toggleFilter(this)"
                                       data-group="Khoảng giá" data-field="sale_price" data-text="Trên 1.000.000đ"
                                       value="(>1000000)" data-operator="OR">
								<i class="fa"></i>
								Giá trên 1.000.000đ
							</label>
						</span>
                        </li>


                    </ul>
                </div>
            </aside>

        </div>
    </div>


</aside>