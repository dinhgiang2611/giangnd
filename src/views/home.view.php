<?php include __DIR__ . '/partials/header.php'; ?>

<?php include __DIR__ . '/partials/home/banner.php'; ?>
<?php include __DIR__ . '/partials/home/cats.php'; ?>
<?php include __DIR__ . '/partials/home/promotes.php'; ?>
<?php include __DIR__ . '/partials/home/product_cat.php'; ?>
<?php include __DIR__ . '/partials/home/partners.php'; ?>
<?php include __DIR__ . '/partials/home/product_items.php'; ?>
<?php include __DIR__ . '/partials/home/news.php'; ?>

<?php include __DIR__ . '/partials/footer.php'; ?>