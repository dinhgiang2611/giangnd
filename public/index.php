<?php

error_reporting(E_ALL);

$root = __DIR__ . '/..';

require $root . '/vendor/autoload.php';

Router::load($root . '/src/app/routes.php')->direct(getUri(), getMethod());
